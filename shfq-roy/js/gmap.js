/* gmap.js */
/* Author: Maxime Déraspe */
/* Create a google map from a table fusion */

var target, output, options, entry, map, layer, tableid;
var selection = new Array();
var markersArray = new Array();
var stylesArray = new Array();
var curOpenBubble = -1;
selection[0] = "0";     // Pour n'avoir rien par défaut

window.onload=init('11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o');

// function to init maps from the main menu
function init(mapid)  {

    tableid = mapid;

    // var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: new google.maps.LatLng(47.25,-70.75),
        // center: new google.maps.LatLng(-33.397, 150.644),
        zoom: 8,
        disableDefaultUI: false,
        navigationControlOptions: {style: google.maps.NavigationControlStyle.big},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    layer = new google.maps.FusionTablesLayer({
        query: {
            select: 'geometry',
            from: tableid,
            where: "'id' in "  + '(' + selection.join(',') + ')'
        },

        styles: [
            {
                // Régions Administrative
                where: "frontiere = 1",
                polylineOptions: {
                    strokeColor: "#000000",
                    strokeWeight: "int",
                    clickable: false
                }
            },
            {
                // Région Naturelle
                where: "frontiere = 2",
                polylineOptions: {
                    strokeColor: "#07E13E",
                    strokeWeight: "int",
                    clickable: false
                }
            },
            {
                // Aire protégée
                where: "frontiere = 3",                
                polygonOptions: {
                    fillColor: "#99CCFF",
                    strokeColor: "#15672A",
                    strokeWeight: "1",
                    clickable: false
                }
            },
            {
                // Feux de forêt 
                where: "frontiere = 7",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            },
            {
                // Feux de forêt
                where: "frontiere = 8",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            },
            {
                // Feux de forêt
                where: "frontiere = 9",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            }
            
        ]

    });

    layer.setMap(map);

    // setDefaultBorderStyles();

}


// function for the checkboxes in .html
function initcheck (name,id) {

    if(name=="frontier"){

        // Set the frontier on the map with layer.setOptions
        // for Polygones, Polylines KML

        if (document.getElementById(id).checked==true){
            selection.push(id);
            getData(id,"point");            
        }else if(document.getElementById(id).checked==false){
            markersArray[id].setMap(null);
            selection.splice(selection.indexOf(id),1);
        }
        
        layer.setOptions({
            query: {
                select: 'geometry',
                from: tableid,
                where: "'id' in "  + '(' + selection.join(',') + ')'
            }
        });

        // Set the marker from the query of location
        // on <markerlocation> column from gtable fusion
    }else if (name=="frontierGroup"){

        if (document.getElementById(id).checked==true){
            if(id.indexOf(",") != -1){
                var ids = id.split(",");
                for (var i = 0; i<ids.length; i++){
                    selection.push(ids[i]);
                    getData(ids[i],"point");
                }
            }else{
                selection.push(id);
                getData(id,"point");
            }
        }else if(document.getElementById(id).checked==false){
            if(id.indexOf(",") != -1){
                var ids = id.split(",");
                for (var i = 0; i<ids.length; i++){
                    markersArray[ids[i]].setMap(null);
                    selection.splice(selection.indexOf(ids[i]),1);
                }
                
            }else{
                markersArray[id].setMap(null);
                selection.splice(selection.indexOf(id),1);
            }
        }
        
        layer.setOptions({
            query: {
                select: 'geometry',
                from: tableid,
                where: "'id' in "  + '(' + selection.join(',') + ')'
            }
        });

    }else if(name=="point"){     

        if (document.getElementById(id).checked==true){

            if(id.indexOf("..") != -1){
                var ids = id.split("..");
                for (var i=ids[0];i<=ids[1];i++){
                    getData(parseInt(i),name);
                }
            }else{
                getData(id,name);
            }
        }else if(document.getElementById(id).checked==false){

            if(id.indexOf("..") != -1){
                var ids = id.split("..");
                for (var i=ids[0];i<=ids[1];i++){
                    if(markersArray[i].infoBubble.isOpen()){
                        markersArray[i].infoBubble.close();
                    }
                    markersArray[i].setMap(null);
                }
            }else{
                if(markersArray[id].infoBubble.isOpen()){
                    markersArray[id].infoBubble.close();
                }
                markersArray[id].setMap(null);
            }

        }

    }else if(name=="overlay"){

        if (document.getElementById(id).checked==true){
            getData(id,name);
        }else if(document.getElementById(id).checked==false){
            markersArray[id].setMap(null);
        }

    }
    
}


// Fct : make the query and make the appropriate marker
// 	create a hashtable result of the query on the table fusion
function getData (id,typename){
    
    var query = "SELECT id, section, name, geometry, description, images, videos, frontiere, markerlocation, icone FROM " +
        '11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o' + " WHERE id = " + id;

    var encodedQuery = encodeURIComponent(query);

    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/query'];
    url.push('?sql=' + encodedQuery);
    url.push('&key=AIzaSyAGh4VEpPqbOxLE0EivpthyqtjXaeeGdt4');
    url.push('&callback=?');

    var queryHash = {};

    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
        dataType: 'jsonp',
        
        success: function (data) {

            var rows = data['rows'];
            var ftData = document.getElementById('ft-data');

            for (var i in rows) {
                
                queryHash['idrow']  = rows[i][0];
                queryHash['section'] = rows[i][1];
                queryHash['name'] = rows[i][2];
                queryHash['geometry'] = rows[i][3];
                queryHash['description'] = rows[i][4];
                queryHash['images'] = rows[i][5];
                queryHash['videos'] = rows[i][6];
                queryHash['frontiere'] = rows[i][7];
                queryHash['location'] = rows[i][8];
                queryHash['icon'] = rows[i][9];
               

                //Check again for a point or overlay 
                if(typename=="point"){
                    createMarker(id, queryHash);
                }else if (typename=="overlay"){
                    createOverlay(id, queryHash);
                }

            }
        }

    });

}


//Fct: Create a marquer on the map and a nice infoBubble (infoBubble.js)
function createMarker (id, queryHash){

    var loc = queryHash['location'].split(",");
    var newIcon = new google.maps.MarkerImage(queryHash['icon'], null, null, null, new google.maps.Size(21,30));
    var marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(loc[0], loc[1]),
        draggable: false,
        icon: newIcon
    });

    // var infoContent =            
    //     '<div id="content">'+
    //     '<h3>' + queryHash['name'] + '</h3> <br>'+
    //     '<b>' + queryHash['section'] + '</b> <br>' + queryHash['description'] +
    //     '</div>';


    var infoContent =            
        '<div id="content">'+
        '<br>' +
        '<b>' + queryHash['description'] + '</b>' +
        '<br>' +
        '</div>';

    marker.infoBubble = new InfoBubble ({
        maxWidth: 560,
        maxHeight: 500
    });

    marker.infoBubble.addTab('<b>Info</b>', infoContent);

    if (queryHash['images']){
        var imgContent =            
            '<div id="content">'+
            queryHash['images'] +
            '</div>';
        marker.infoBubble.addTab('<b>Images</b>', imgContent);
    }

    if (queryHash['videos']){
        var vidContent =            
            '<div id="content">'+
            queryHash['videos'] +
            '</div>';
        marker.infoBubble.addTab('<b>Vidéos</b>', queryHash['videos']);
    }
    
    // // Open directly the infoBubble instead of the listener
    // marker.infoBubble.open(map, marker);

    // add the listener for the click
    google.maps.event.addListener(marker, 'click', function() {
        if (!marker.infoBubble.isOpen()) {

            marker.infoBubble.open(map, marker);

            // if (markersArray[curOpenBubble].infoBubble.isOpen()){
            //     markersArray[curOpenBubble].infoBubble.close();
            // }
            // curOpenBubble = id;
        }
    });
    
    markersArray[id] = marker;
}



// Fct: Create a groundoverlay, image that overlay the map
// 	.. historical map that lay over the google map.
function createOverlay (id, queryHash){

    var loc = queryHash['location'].split(";");

    // Coordinates need to be like that in makersLocations field
    // from google table.
    // south,west;north,est
    var south = parseFloat(loc[0].split(",")[0]);
    var west = parseFloat(loc[0].split(",")[1]);
    var north = parseFloat(loc[1].split(",")[0]);
    var est = parseFloat(loc[1].split(",")[1]);


    var imageBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(south,west),    
    new google.maps.LatLng(north,est));

    var oldmap = new google.maps.GroundOverlay(
        queryHash['images'],
        imageBounds);

    oldmap.setMap(map);
    
    markersArray[id] = oldmap;
}



function setDefaultBorderStyles () {
    
    var query = "SELECT frontiere, couleur-contour FROM " +
        '11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o' + " WHERE id = *";

    var encodedQuery = encodeURIComponent(query);

    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/query'];
    url.push('?sql=' + encodedQuery);
    url.push('&key=AIzaSyAGh4VEpPqbOxLE0EivpthyqtjXaeeGdt4');
    url.push('&callback=?');

    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
        dataType: 'jsonp',
        
        success: function (data) {

            var rows = data['rows'];
            var ftData = document.getElementById('ft-data');

            for (var i in rows) {                
                var frontiere = rows[i][0];
                var color = rows[i][1];
            }
        }

    });

    // alert("HI");
    // alert(query);
    alert(frontiere);
    alert(color);
    setStyleOptions(frontiere,color,"border");
}


function setStyleOptions (frontiere, color, style){

    // Set The Region Frontier
    // layer = new google.maps.FusionTablesLayer({ query, style })
    layer = new google.maps.FusionTablesLayer({
        query: {
            select: 'geometry',
            from: tableid,
            where: "'id' in "  + '(' + selection.join(',') + ')'
        },
        styles: [
            {
                // Border options
                where: "frontiere = " + frontiere,
                polylineOptions: {
                    strokeColor: color,
                    strokeWeight: "int"  
                }
            }
        ]
    });
    layer.setMap(map);

}
