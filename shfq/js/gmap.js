/* gmap.js */
/* Author: Maxime Déraspe */
/* Create a google map from a table fusion */

var target, output, options, entry, map, layer, tableid;
var selection = new Array();
var markersArray = new Array();
var stylesArray = new Array();
var curOpenBubble = -1;
selection[0] = "0";     // Pour n'avoir rien par défaut

window.onload=init('11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o');

// function to init maps from the main menu
function init(mapid)  {

    tableid = mapid;

    // var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: new google.maps.LatLng(47.25,-70.75),
        // center: new google.maps.LatLng(-33.397, 150.644),
        zoom: 9,
        disableDefaultUI: false,
        navigationControlOptions: {style: google.maps.NavigationControlStyle.big},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    layer = new google.maps.FusionTablesLayer({
        query: {
            select: 'geometry',
            from: tableid,
            where: "'id' in "  + '(' + selection.join(',') + ')'
        },

        styles: [
            {
                // Régions Administrative
                where: "frontiere = 1",
                polylineOptions: {
                    strokeColor: "#000000",
                    strokeWeight: "int",
                    clickable:false
                }
            },
            {
                // Région Naturelle
                where: "frontiere = 2",
                polylineOptions: {
                    strokeColor: "#07E13E",
                    strokeWeight: "int",
                    clickable:false
                }
            },
            {
                // Aire protégée
                where: "frontiere = 3",                
                polygonOptions: {
                    fillColor: "#99CCFF",
                    strokeColor: "#15672A",
                    strokeWeight: "1",
                    clickable:false,
                    draggable:true
                }
            },
            {
                // Feux de forêt 
                where: "frontiere = 7",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            },
            {
                // Feux de forêt
                where: "frontiere = 8",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            },
            {
                // Feux de forêt
                where: "frontiere = 9",                
                polygonOptions: {
                    fillColor: "#C80000",
                    strokeColor: "#000000",
                    strokeWeight: "1"
                }
            }

        ]

    });

    layer.setMap(map);

    // As soon as it works (2 bottom queries)
    // setDefaultBorderStyles();

    // Set Régions Administratives on load 
    for (var i = 1; i < 8 ; i++) {
        toggleCheckBox(i);
        initcheck("frontier",i.toString());
    }
}


// function for the checkboxes in .html
function initcheck (name,id) {
    
    //  Get an array of IDs
    if(id.indexOf(",") != -1){
        var ids = id.split(",");
    }else if(id.indexOf("..") != -1){
        var tmpid = id.split("..");
        var j = 0;
        var ids = [];
        for (var i=tmpid[0];i<=tmpid[1];i++){
            ids[j] = i;
            j = j + 1;
        }
    }else{
        var ids = [];
        ids.push(id);
    }

    

    // Get the layout you want
    if(name=="frontier"){
        // Set the frontier on the map with layer.setOptions
        // for Polygones, Polylines KML

        if (document.getElementById(id).checked==true){
            for (var i=0;i<ids.length;i++){
                selection.push(ids[i]);
                getData(ids[i],"point");           
            }
        }else if(document.getElementById(id).checked==false){
            for (var i=0;i<ids.length;i++){
                markersArray[ids[i]].setMap(null);
                markersArray[ids[i]].infoBubble.close();
                selection.splice(selection.indexOf(ids[i]),1);
            }
        }

        layer.setOptions({
            query: {
                select: 'geometry',
                from: tableid,
                where: "'id' in "  + '(' + selection.join(',') + ')'
            },
            clickable:false
        });

    }else if(name=="point"){     

        if (document.getElementById(id).checked==true){
            for(var i=0;i<ids.length;i++){
                getData(ids[i],name);
            }
        }else if(document.getElementById(id).checked==false){
            for(var i=0;i<ids.length;i++){
                markersArray[ids[i]].setMap(null);
                markersArray[ids[i]].infoBubble.close();
            }
        }

    }else if(name=="overlay"){

        if (document.getElementById(id).checked==true){
            for(var i=0;i<ids.length;i++){
                getData(ids[i],name);   
            }
        }else if(document.getElementById(id).checked==false){
            for(var i=0;i<ids.length;i++){
                markersArray[ids[i]].setMap(null);
                markersArray[ids[i]].infoBubble.close();
            }
        }

    }
    
}


// Fct : make the query and make the appropriate marker
// 	create a hashtable result of the query on the table fusion
function getData (id,typename){
    
    var query = "SELECT id, section, name, geometry, description, images, videos, frontiere, markerlocation, icone FROM " +
        '11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o' + " WHERE id = " + id;

    var encodedQuery = encodeURIComponent(query);

    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/query'];
    url.push('?sql=' + encodedQuery);
    url.push('&key=AIzaSyAGh4VEpPqbOxLE0EivpthyqtjXaeeGdt4');
    url.push('&callback=?');

    var queryHash = {};

    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
        dataType: 'jsonp',
        
        success: function (data) {

            var rows = data['rows'];
            var ftData = document.getElementById('ft-data');

            for (var i in rows) {
                
                queryHash['idrow']  = rows[i][0];
                queryHash['section'] = rows[i][1];
                queryHash['name'] = rows[i][2];
                queryHash['geometry'] = rows[i][3];
                queryHash['description'] = rows[i][4];
                queryHash['images'] = rows[i][5];
                queryHash['videos'] = rows[i][6];
                queryHash['frontiere'] = rows[i][7];
                queryHash['location'] = rows[i][8];
                queryHash['icon'] = rows[i][9];
               

                //Check again for a point or overlay 
                if(typename=="point"){
                    createMarker(id, queryHash);
                }else if (typename=="overlay"){
                    createOverlay(id, queryHash);
                }

            }
        }

    });

}


//Fct: Create a marquer on the map and a nice infoBubble (infoBubble.js)
function createMarker (id, queryHash){

    var loc = queryHash['location'].split(",");
    var newIcon = new google.maps.MarkerImage(queryHash['icon'], null, null, null, new google.maps.Size(60,60));
    var marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(loc[0], loc[1]),
        draggable: false,
        icon: newIcon
    });

    // var infoContent =            
    //     '<div id="content">'+
    //     '<h3>' + queryHash['name'] + '</h3> <br>'+
    //     '<b>' + queryHash['section'] + '</b> <br>' + queryHash['description'] +
    //     '</div>';


    var infoContent =            
        '<div id="infobubble">'+
        '<br>' +
        '<b>' + queryHash['description'] + '</b>' +
        '<br>' +
        '</div>';

    marker.infoBubble = new InfoBubble ({
        maxWidth: 560,
        maxHeight: 500
    });

    marker.infoBubble.addTab('<b>Info</b>', infoContent);

    if (queryHash['images']){
        var imgContent =            
            '<div id="infobubble">'+
            queryHash['images'] +
            '</div>';
        marker.infoBubble.addTab('<b>Images</b>', imgContent);
    }

    if (queryHash['videos']){
        var vidContent =            
            '<div id="infobubble">'+
            queryHash['videos'] +
            '</div>';
        marker.infoBubble.addTab('<b>Vidéo</b>', queryHash['videos']);
    }
    
    // // Open directly the infoBubble instead of the listener
    // marker.infoBubble.open(map, marker);

    // add the listener for the click
    google.maps.event.addListener(marker, 'click', function() {
        if (!marker.infoBubble.isOpen()) {

            marker.infoBubble.open(map, marker);

            // if (markersArray[curOpenBubble].infoBubble.isOpen()){
            //     markersArray[curOpenBubble].infoBubble.close();
            // }
            // curOpenBubble = id;
        }
    });
    
    markersArray[id] = marker;
}



// Fct: Create a groundoverlay, image that overlay the map
// 	.. historical map that lay over the google map.
function createOverlay (id, queryHash){

    var loc = queryHash['location'].split(";");

    // Coordinates need to be like that in makersLocations field
    // from google table.
    // south,west;north,est
    var south = parseFloat(loc[0].split(",")[0]);
    var west = parseFloat(loc[0].split(",")[1]);
    var north = parseFloat(loc[1].split(",")[0]);
    var est = parseFloat(loc[1].split(",")[1]);


    var imageBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(south,west),    
    new google.maps.LatLng(north,est));

    var oldmap = new google.maps.GroundOverlay(
        queryHash['images'],
        imageBounds);

    oldmap.setMap(map);
    
    markersArray[id] = oldmap;
}

//Fct: Toggle a checkbox on or off
function toggleCheckBox (id){
    var elementCheckBox = document.getElementById(id);

    if (elementCheckBox.checked == true){
        elementCheckBox.checked = false;
    }else{
        elementCheckBox.checked = true;
    }

}


// ##### Aboves function
// Automaticyly create style for frontiere with border colors
// NOT WORKING FOR NOW .... HARD CODED IN THE BEGINNING
// #############################################################


function setDefaultBorderStyles () {
    
    var query = "SELECT frontiere, couleur-contour FROM " +
        '11IiZm1Ehb_VfOQWpkOP6Dhz6zKle-b7HEy_KA-o' + " WHERE id = *";

    var encodedQuery = encodeURIComponent(query);

    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/query'];
    url.push('?sql=' + encodedQuery);
    url.push('&key=AIzaSyAGh4VEpPqbOxLE0EivpthyqtjXaeeGdt4');
    url.push('&callback=?');

    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
        dataType: 'jsonp',
        
        success: function (data) {

            var rows = data['rows'];
            var ftData = document.getElementById('ft-data');

            for (var i in rows) {                
                var frontiere = rows[i][0];
                var color = rows[i][1];
            }
        }

    });

    // alert("HI");
    // alert(query);
    alert(frontiere);
    alert(color);
    setStyleOptions(frontiere,color,"border");
}


function setStyleOptions (frontiere, color, style){

    // Set The Region Frontier
    // layer = new google.maps.FusionTablesLayer({ query, style })
    layer = new google.maps.FusionTablesLayer({
        query: {
            select: 'geometry',
            from: tableid,
            where: "'id' in "  + '(' + selection.join(',') + ')'
        },
        styles: [
            {
                // Border options
                where: "frontiere = " + frontiere,
                polylineOptions: {
                    strokeColor: color,
                    strokeWeight: "int"
                }
            }
        ]
    });
    layer.setMap(map);

}


function sleep(ms)
{
    var dt = new Date();
    dt.setTime(dt.getTime() + ms);
    while (new Date().getTime() < dt.getTime());
}
